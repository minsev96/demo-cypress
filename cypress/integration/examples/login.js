exports.login=function(test_data){

    cy.visit('https://github.com/login');

    //input email    
    cy.get('input[name="login"]').type(test_data.email);
    
    //input password
    cy.get('input[name="password"]').clear().type(test_data.password);
    
    //click button Masuk
    cy.get('input[name="commit"]').click({force:true});

    cy.get('[class="flash flash-full flash-error "]', {timeout:5000}).contains(test_data.message_error)
        .should('be.visible');
};