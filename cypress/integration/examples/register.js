exports.register=function(test_data){

    cy.visit('https://github.com/');

    cy.get('div.d-flex.flex-justify-between > .d-flex > .d-inline-block').click();

    //input username    
    cy.get('input[name="user[login]"]').type(test_data.username);
    
    //input email
    cy.get('input[name="user[email]"]').type(test_data.email);

    //input password
    cy.get('input[name="user[password]"]').type(test_data.password);
    
    //click button Masuk
    cy.get('[type="submit"]').should('be.disabled');
};