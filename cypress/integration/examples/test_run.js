import { login } from "./login"
import { register } from "./register"

describe("Demo Login", function(){
    beforeEach(() => {
      })
    
    it("As A User I want to Login Github with Wrong Password", function(){
        var test_data =
        {
            "email" : "hilmiaulia96@gmail.com",
            "password" : "123456",
            "message_error": "Incorrect username or password."
        };
        login(test_data);
    });

    it("As A User I want to Register Github", function(){
        var test_data =
        {
            "username": "hilmiaulia123455",
            "email" : "hilmiaulia11111@gmail.com",
            "password" : "Hiuhqiadq90982"
        };
        register(test_data);
    });

});